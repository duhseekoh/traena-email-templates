# Traena Email Template (Built with Zurb)

This repo was created using the foundation-emails project:
* Main repo: http://github.com/zurb/foundation-emails/
* Starter kit repo: https://github.com/zurb/foundation-emails-template

At Traena we are using this project to create all of our emails. By using foundation emails we get cross-client compatible email templates.

## Build Commands

Run `npm start` to kick off the build process. A new browser tab will open with a server pointing to your project files.

Run `npm run build` to inline your CSS into your HTML along with the rest of the build process.

Run `npm run litmus` to build as above, then submit to litmus for testing. *AWS S3 Account details required (config.json)*

Run `npm run mail` to build as above, then send to specified email address for testing. *SMTP server details required (config.json)*

Run `npm run zip` to build as above, then zip HTML and images for easy deployment to email marketing services.

Run `npm run send-welcome-email -- --to {EMAIL}` || `npm run send-rest-email -- --to {EMAIL}` to test sending the email 

### Archives

Emails that came out of the box which are are not using are stored in `src/pages/archive`. They don't end up getting built in the build process. We can also retire unused email templates there.
Same thing for images stored at `src/assets/img/archive`.

## Creating templates

### Create a new template

Copy one of the existing templates from `src/pages` and make modifications to it.

### Variables

Auth0 expects variables to be defined in emails by surrounding variable expressions with curly braces. e.g. `<h1>{{application.name}}</h1>`. In order to get this syntax outputted into the email templates, the curly braces must be escaped like this:
```
<h1>\{{application.name}</h1>
```
This prevents zurb from attempting its own variable interpolation (https://github.com/zurb/inky/issues/28).

## Using the templates

This project handles creation of email templates, but nothing beyond that. To use these elsewhere in the Traena ecosystem, the built emails must be copied to another location.

### auth0

To use these email templates in auth0 run the build command and copy the html directly from the built html files from `dist/*.html` to the relevant email body on the auth0 emails page: https://manage.auth0.com/#/emails. This needs to be done for each of the auth0 environments.

* https://traena-dev.auth0.com
* https://traena-staging.auth0.com
* https://traena-production.auth0.com

### traena-api

To use these email templates from the traena-api, run the build command and copy the individual .html files from `dist/*.html` to the email templates folder (TODO) in the https://github.com/Traena/traena-api repo.

## Litmus Tests (config.json)

Testing in Litmus requires the images to be hosted publicly. The provided gulp task handles this by automating hosting to an AWS S3 account. Provide your Litmus and AWS S3 account details in the `example.config.json` and then rename to `config.json`. Litmus config, and `aws.url` are required, however if you follow the [aws-sdk suggestions](http://docs.aws.amazon.com/AWSJavaScriptSDK/guide/node-configuring.html) you don't need to supply the AWS credentials into this JSON.

```json
{
  "aws": {
    "region": "us-east-1",
    "accessKeyId": "YOUR_ACCOUNT_KEY",
    "secretAccessKey": "YOUR_ACCOUNT_SECRET",
    "params": {
        "Bucket": "elasticbeanstalk-us-east-1-THIS_IS_JUST_AN_EXAMPLE"
    },
    "url": "https://s3.amazonaws.com/elasticbeanstalk-us-east-1-THIS_IS_JUST_AN_EXAMPLE"
  },
  "litmus": {
    "username": "YOUR_LITMUS@EMAIL.com",
    "password": "YOUR_ACCOUNT_PASSWORD",
    "url": "https://YOUR_ACCOUNT.litmus.com",
    "applications": ["ol2003","ol2007","ol2010","ol2011","ol2013","chromegmailnew","chromeyahoo","appmail9","iphone5s","ipad","android4","androidgmailapp"]
  }
}
```

## Manual email tests (config.json)

Similar to the Litmus tests, you can have the emails sent to a specified email address. Just like with the Litmus tests, you will need to provide AWS S3 account details in `config.json`. You will also need to specify to details of an SMTP server. The email address to send to emails to can either by configured in the `package.json` file or added as a parameter like so: `npm run mail -- --to="example.com"`

```json
{
  "aws": {
    "region": "us-east-1",
    "accessKeyId": "YOUR_ACCOUNT_KEY",
    "secretAccessKey": "YOUR_ACCOUNT_SECRET",
    "params": {
        "Bucket": "elasticbeanstalk-us-east-1-THIS_IS_JUST_AN_EXAMPLE"
    },
    "url": "https://s3.amazonaws.com/elasticbeanstalk-us-east-1-THIS_IS_JUST_AN_EXAMPLE"
  },
  "mail": {
    "to": [
      "example@domain.com"
    ],
    "from": "Company name <info@company.com",
    "smtp": {
      "auth": {
        "user": "example@domain.com",
        "pass": "12345678"
      },
      "host": "smtp.domain.com",
      "secureConnection": true,
      "port": 465
    }
  }
}
```

For a full list of Litmus' supported test clients(applications) see their [client list](https://litmus.com/emails/clients.xml).
